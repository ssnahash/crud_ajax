from django.conf.urls import url, include
from .views import book_list, book_create, book_update, book_delete

urlpatterns = [
    url(r'^$', book_list, name='book_list'),
    url(r'^books/$', book_list, name='book_list'),
    url(r'^books/create/$', book_create, name='book_create'),
    url(r'^books/(?P<pk>\d+)/update/$', book_update, name='book_update'),
    url(r'^books/(?P<pk>\d+)/delete/$', book_delete, name='book_delete'),
]